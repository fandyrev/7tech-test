package se.rocketscien.test.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.jdbc.Sql;
import se.rocketscien.test.dto.MoneyAccountDTO;
import se.rocketscien.test.dto.MoneyRequestDto;
import se.rocketscien.test.dto.ResponseDto;
import se.rocketscien.test.repository.MoneyAccountRepository;
import se.rocketscien.test.service.MoneyService;

import java.math.BigDecimal;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MoneyControllerTest {

    private volatile int threadsRun = 0;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private MoneyService moneyService;

    @Autowired
    MoneyAccountRepository moneyAccountRepository;

    @BeforeEach
    public void setup() {
        testRestTemplate.getRestTemplate().setRequestFactory(new HttpComponentsClientHttpRequestFactory());
    }

    @ParameterizedTest
    @MethodSource("putMoneyProvider")
    void putMoneyException(String id, HttpStatus status, BigDecimal money, String message) {
        MoneyRequestDto dto = new MoneyRequestDto();
        dto.setMoney(money);
        HttpEntity<MoneyRequestDto> entity = new HttpEntity<MoneyRequestDto>(dto, null);
        errorResponse("/money/" + id + "/replenishment", status, message, entity);
    }

    private static Stream<Arguments> putMoneyProvider() {
        return Stream.of(
                Arguments.of("5", HttpStatus.NOT_FOUND, BigDecimal.valueOf(1), "account with id = 5 not found"),
                Arguments.of("money", HttpStatus.BAD_REQUEST, BigDecimal.valueOf(1), "expected value of type: long"),
                Arguments.of("-4", HttpStatus.BAD_REQUEST, BigDecimal.valueOf(1), "id must be greater than 0"),
                Arguments.of("1", HttpStatus.BAD_REQUEST, BigDecimal.valueOf(-1), "money must be greater than 0")
        );
    }

    @Test
    @Sql("/db/test-clean-data.sql")
    void putMoney() {
        MoneyRequestDto dto = new MoneyRequestDto();
        dto.setMoney(BigDecimal.valueOf(1.01));
        HttpEntity<MoneyRequestDto> entity = new HttpEntity<MoneyRequestDto>(dto, null);
        ResponseEntity<ResponseDto<MoneyAccountDTO>> response = testRestTemplate.exchange("/money/1/replenishment",
                HttpMethod.PATCH,
                entity,
                new ParameterizedTypeReference<ResponseDto<MoneyAccountDTO>>() {});

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody().getStatus(), equalTo(ResponseDto.Status.OK));
        assertEquals(new MoneyAccountDTO(1L, BigDecimal.valueOf(1.01)), response.getBody().getData());
    }

    @ParameterizedTest
    @MethodSource("withdrawMoneyProvider")
    void withdrawMoneyException(String id, HttpStatus status, BigDecimal money, String message) {
        MoneyRequestDto dto = new MoneyRequestDto();
        dto.setMoney(money);
        HttpEntity<MoneyRequestDto> entity = new HttpEntity<MoneyRequestDto>(dto, null);
        errorResponse("/money/" + id + "/withdrawal", status, message, entity);
    }

    private static Stream<Arguments> withdrawMoneyProvider() {
        return Stream.of(
                Arguments.of("5", HttpStatus.NOT_FOUND, BigDecimal.valueOf(1), "account with id = 5 not found"),
                Arguments.of("money", HttpStatus.BAD_REQUEST, BigDecimal.valueOf(1), "expected value of type: long"),
                Arguments.of("-4", HttpStatus.BAD_REQUEST, BigDecimal.valueOf(1), "id must be greater than 0"),
                Arguments.of("1", HttpStatus.BAD_REQUEST, BigDecimal.valueOf(-1), "money must be greater than 0"),
                Arguments.of("1", HttpStatus.BAD_REQUEST, BigDecimal.valueOf(1), "money not enough in account = 1")
        );
    }

    @Test
    @Sql("/db/test-clean-data.sql")
    void withdrawMoney() {
        MoneyRequestDto dto = new MoneyRequestDto();
        dto.setMoney(BigDecimal.valueOf(0.99));
        HttpEntity<MoneyRequestDto> entity = new HttpEntity<MoneyRequestDto>(dto, null);
        ResponseEntity<ResponseDto<MoneyAccountDTO>> response = testRestTemplate.exchange("/money/2/withdrawal",
                HttpMethod.PATCH,
                entity,
                new ParameterizedTypeReference<ResponseDto<MoneyAccountDTO>>() {});

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody().getStatus(), equalTo(ResponseDto.Status.OK));
        assertEquals(new MoneyAccountDTO(2L, BigDecimal.valueOf(1.01)), response.getBody().getData());
    }

    @ParameterizedTest
    @MethodSource("moveMoneyProvider")
    void moveMoneyException(String idFrom, String idTo, HttpStatus status, BigDecimal money, String message) {
        MoneyRequestDto dto = new MoneyRequestDto();
        dto.setMoney(money);
        HttpEntity<MoneyRequestDto> entity = new HttpEntity<MoneyRequestDto>(dto, null);
        errorResponse("/money/" + idFrom + "/move/" + idTo, status, message, entity);
    }

    private static Stream<Arguments> moveMoneyProvider() {
        return Stream.of(
                Arguments.of("5", "1", HttpStatus.NOT_FOUND, BigDecimal.valueOf(1), "account with id = 5 not found"),
                Arguments.of("money", "1", HttpStatus.BAD_REQUEST, BigDecimal.valueOf(1), "expected value of type: long"),
                Arguments.of("-4", "1", HttpStatus.BAD_REQUEST, BigDecimal.valueOf(1), "fromId must be greater than 0"),
                Arguments.of("2", "5", HttpStatus.NOT_FOUND, BigDecimal.valueOf(1), "account with id = 5 not found"),
                Arguments.of("2", "money", HttpStatus.BAD_REQUEST, BigDecimal.valueOf(1), "expected value of type: long"),
                Arguments.of("2", "-4", HttpStatus.BAD_REQUEST, BigDecimal.valueOf(1), "toId must be greater than 0"),
                Arguments.of("2", "1", HttpStatus.BAD_REQUEST, BigDecimal.valueOf(-1), "money must be greater than 0"),
                Arguments.of("2", "1", HttpStatus.BAD_REQUEST, BigDecimal.valueOf(4), "money not enough in account = 2")
        );
    }

    @Test
    @Sql("/db/test-clean-data.sql")
    void moveMoney() {
        MoneyRequestDto dto = new MoneyRequestDto();
        dto.setMoney(BigDecimal.valueOf(0.99));
        HttpEntity<MoneyRequestDto> entity = new HttpEntity<MoneyRequestDto>(dto, null);
        ResponseEntity<ResponseDto<MoneyAccountDTO>> response = testRestTemplate.exchange("/money/2/move/1",
                HttpMethod.PATCH,
                entity,
                new ParameterizedTypeReference<ResponseDto<MoneyAccountDTO>>() {});

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody().getStatus(), equalTo(ResponseDto.Status.OK));
    }

    @Test
    @Sql({"/db/test-clean-data.sql", "/db/test-multi-thread-data.sql"})
    void moveMoneyConcurrent() {
        final int TEST_THREAD_COUNT = 2;
        final ExecutorService pool = Executors.newFixedThreadPool(TEST_THREAD_COUNT);

        class MoveMoneyRunnable implements Runnable{

            private Long fromId;

            private Long toId;

            private BigDecimal money;

            public MoveMoneyRunnable(Long fromId, Long toId, BigDecimal money) {
                this.fromId = fromId;
                this.toId = toId;
                this.money = money;
            }

            @Override
            public void run() {
                for (int i = 1; i <= 100; i++) {
                    moneyService.transferringMoney(fromId, toId, money);
                }

                threadsRun++;
                if (threadsRun >= TEST_THREAD_COUNT) {
                    pool.shutdown();
                }
            }
        }

        Runnable moveFrom2 = new MoveMoneyRunnable(2l, 1l, BigDecimal.valueOf(1));
        Runnable moveFrom1 = new MoveMoneyRunnable(1l, 2l, BigDecimal.valueOf(2));

        pool.submit(moveFrom2);
        pool.submit(moveFrom1);

        while (! pool.isShutdown())
        {
        }

        assertEquals(BigDecimal.valueOf(500.00).setScale(2), moneyAccountRepository.getById(2L).getMoney());
        assertEquals(BigDecimal.valueOf(300.00).setScale(2), moneyAccountRepository.getById(1L).getMoney());
    }

    private void errorResponse(String url, HttpStatus status, String errorMessage, HttpEntity entity) {
        ResponseEntity<ResponseDto<?>> response = testRestTemplate.exchange(url,
                HttpMethod.PATCH,
                entity,
                new ParameterizedTypeReference<ResponseDto<?>>() {});
        assertThat(response.getStatusCode(), equalTo(status));
        assertThat(response.getBody().getStatus(), equalTo(ResponseDto.Status.ERROR));
        assertThat(response.getBody().getError(), is(notNullValue()));
        if(errorMessage != null) {
            assertThat(response.getBody().getError().getMessage(), equalTo(errorMessage));
        }
    }
}
