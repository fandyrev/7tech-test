CREATE TABLE money_account (
                                id IDENTITY,
                                money DECIMAL(19, 2) DEFAULT 0,
                                CONSTRAINT money_account_pkey PRIMARY KEY (id)
);
