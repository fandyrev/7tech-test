package se.rocketscien.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;
import se.rocketscien.test.model.MoneyAccount;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Optional;

@Repository
public interface MoneyAccountRepository extends JpaRepository<MoneyAccount, Long> {
    @Lock(LockModeType.PESSIMISTIC_READ)
    Optional<MoneyAccount> findById(Long id);
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    MoneyAccount save(MoneyAccount entity);
    @Lock(LockModeType.PESSIMISTIC_READ)
    List<MoneyAccount> findByIdIn(List<Long> ids);
}
