package se.rocketscien.test.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
@Schema(description = "Money request")
public class MoneyRequestDto {

    @Positive
    @Schema(description = "Money", example = "1.00")
    private BigDecimal money;
}
