package se.rocketscien.test.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Value
@Schema(description = "Money account")
public class MoneyAccountDTO {

    @Schema(description = "Id", example = "1")
    private Long id;

    @NotNull
    @Schema(description = "Money", example = "1.00")
    private BigDecimal money;
}
