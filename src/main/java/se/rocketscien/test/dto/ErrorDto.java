package se.rocketscien.test.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@NoArgsConstructor
@Schema(description = "Api error response")
public class ErrorDto {

    @Schema(description = "Error response message", example = "MoneyAccount with id = 5 not exist")
    private String message;

    @Schema(description = "Error response timestamp", example = "2021-10-15T05:30:57.451Z")
    private Instant timestamp;

    public ErrorDto(String message) {
        this.message = message;
        timestamp = Instant.now();
    }

    public ErrorDto(String message, Instant timestamp) {
        this.message = message;
        this.timestamp = timestamp;
    }
}
