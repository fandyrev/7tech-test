package se.rocketscien.test.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Api response")
public class ResponseDto<T> {

    @Schema(description = "Response data", example = "{\"id\": \"1\", \"money\": \"1.00\"}")
    private T data;

    @Schema(description = "Response status", example = "ERROR")
    private Status status;

    private ErrorDto error;

    public enum Status {OK, ERROR};

    public static <T> ResponseDto<T> ok (T data) {
        return new ResponseDto<T>(data, Status.OK, null);
    }

    public static ResponseDto<Void> ok () {
        return new ResponseDto<Void>(null, Status.OK, null);
    }

    public static ResponseDto<Void> error (ErrorDto error) {
        return new ResponseDto<Void>(null, Status.ERROR, error);
    }
}

