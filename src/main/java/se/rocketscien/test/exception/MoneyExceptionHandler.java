package se.rocketscien.test.exception;

import com.google.common.base.CaseFormat;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import se.rocketscien.test.dto.ErrorDto;
import se.rocketscien.test.dto.ResponseDto;

import javax.validation.ConstraintViolationException;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
@RequiredArgsConstructor
public class MoneyExceptionHandler {

    private final MessageSource messageSource;

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<ResponseDto> handleApiException(ApiException e){
        return createErrorResponse(getMessage(e.getErrorCode(), e.getArgs()), e.getErrorStatus());
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity<ResponseDto> handleBindException(BindException e){
        String errorCodePrefix = e.getTarget().getClass().getSimpleName() + ".";
        String errors = e.getBindingResult().getFieldErrors().stream().map(fieldError -> {
            String errorCode = errorCodePrefix + fieldError.getField() + "." + fieldError.getCode();
            return getMessage(errorCode,null);
        }).collect(Collectors.joining(", "));
        return createErrorResponse(errors, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ResponseDto> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e){
        String errorCode = e.getParameter().getExecutable().getDeclaringClass().getSimpleName() + "." +
                e.getParameter().getExecutable().getName() + "." +
                e.getParameter().getParameterName() + "." +
                e.getErrorCode();
        return createErrorResponse(getMessage(errorCode, null), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ResponseDto> handleValidationException(ConstraintViolationException e){
        String errors = e.getConstraintViolations().stream().map(constraintViolation ->{
            String errorCode = constraintViolation.getRootBeanClass().getSimpleName() + "." +
                    constraintViolation.getPropertyPath() + "." +
                    constraintViolation.getConstraintDescriptor().getAnnotation().annotationType().getName();
            return getMessage(errorCode, null);
        }).collect(Collectors.joining(", "));
        return createErrorResponse(errors, HttpStatus.BAD_REQUEST);
    }


    private ResponseEntity createErrorResponse (String message, HttpStatus status) {
        log.error(message);
        ErrorDto error = new ErrorDto(message);
        return new ResponseEntity(ResponseDto.error(error), status);
    }

    private String getMessage(String errorCode, Object[] args) {
        errorCode = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, errorCode).replaceAll("\\._", "\\.");
        return messageSource.getMessage(errorCode,args, Locale.getDefault());
    }
}
