package se.rocketscien.test.exception;

import org.springframework.http.HttpStatus;

public class AccountNotFoundException extends ApiException {

    public AccountNotFoundException(String message, Long productId) {
        super(message,
                HttpStatus.NOT_FOUND,
                "Exception.MoneyAccount.AccountNotFound",
                new Object[]{productId});
    }
}
