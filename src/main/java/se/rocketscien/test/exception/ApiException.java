package se.rocketscien.test.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ApiException extends RuntimeException {
    protected HttpStatus errorStatus;
    protected String errorCode;
    protected Object[] args;

    public ApiException(String message, HttpStatus errorStatus, String errorCode, Object[] args) {
        super(message);
        this.errorStatus = errorStatus;
        this.errorCode = errorCode;
        this.args = args;
    }
}
