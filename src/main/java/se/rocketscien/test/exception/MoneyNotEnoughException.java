package se.rocketscien.test.exception;

import org.springframework.http.HttpStatus;

public class MoneyNotEnoughException extends ApiException {

    public MoneyNotEnoughException(String message, Long productId) {
        super(message,
                HttpStatus.BAD_REQUEST,
                "Exception.Money.MoneyNotEnough",
                new Object[]{productId});
    }
}
