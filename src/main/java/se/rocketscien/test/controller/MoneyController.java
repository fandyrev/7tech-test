package se.rocketscien.test.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import se.rocketscien.test.converter.MoneyAccountConverter;
import se.rocketscien.test.dto.MoneyAccountDTO;
import se.rocketscien.test.dto.MoneyRequestDto;
import se.rocketscien.test.dto.ResponseDto;
import se.rocketscien.test.model.MoneyAccount;
import se.rocketscien.test.service.MoneyService;

import javax.validation.Valid;
import javax.validation.constraints.Positive;

@RestController
@RequestMapping("money")
@Validated
@RequiredArgsConstructor
public class MoneyController {
    private final MoneyService moneyService;
    private final MoneyAccountConverter moneyAccountConverter;

    @PatchMapping("/{id}/replenishment")
    @Operation(summary = "Put money", description = "To put money into the account")
    public ResponseDto<MoneyAccountDTO> putMoney(
            @PathVariable("id")
            @Positive
                    Long id,
            @Valid
            @RequestBody
                    MoneyRequestDto money){
        MoneyAccount moneyAccount = moneyService.putMoney(id, money.getMoney());
        return ResponseDto.ok(moneyAccountConverter.toMoneyAccountDTO(moneyAccount));
    }

    @PatchMapping("/{id}/withdrawal")
    @Operation(summary = "Withdraw money", description = "Withdraw money from the account")
    public ResponseDto<MoneyAccountDTO> withdrawMoney(
            @PathVariable("id")
            @Positive
                    Long id,
            @Valid
            @RequestBody
                    MoneyRequestDto money){
        MoneyAccount moneyAccount = moneyService.withdrawMoney(id, money.getMoney());
        return ResponseDto.ok(moneyAccountConverter.toMoneyAccountDTO(moneyAccount));
    }

    @PatchMapping("/{fromId}/move/{toId}")
    @Operation(summary = "Transferring money", description = "Transferring money from one account to another")
    public ResponseDto transferringMoney(
            @PathVariable("fromId")
            @Positive
                    Long fromId,
            @PathVariable("toId")
            @Positive
                    Long toId,
            @Valid
            @RequestBody
                    MoneyRequestDto money){
        moneyService.transferringMoney(fromId, toId, money.getMoney());
        return ResponseDto.ok();
    }
}
