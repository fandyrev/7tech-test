package se.rocketscien.test.converter;

import org.mapstruct.Mapper;
import se.rocketscien.test.dto.MoneyAccountDTO;
import se.rocketscien.test.model.MoneyAccount;

@Mapper(componentModel = "spring")
public interface MoneyAccountConverter {
    MoneyAccountDTO toMoneyAccountDTO(MoneyAccount moneyAccount);
}
