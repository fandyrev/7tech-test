package se.rocketscien.test.service;

import se.rocketscien.test.model.MoneyAccount;

import java.math.BigDecimal;

public interface MoneyService {
    MoneyAccount putMoney(Long id, BigDecimal money);
    MoneyAccount withdrawMoney(Long id, BigDecimal money);
    void transferringMoney(Long fromId, Long toId, BigDecimal money);
}
