package se.rocketscien.test.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.rocketscien.test.exception.AccountNotFoundException;
import se.rocketscien.test.exception.MoneyNotEnoughException;
import se.rocketscien.test.model.MoneyAccount;
import se.rocketscien.test.repository.MoneyAccountRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class MoneyServiceImpl implements MoneyService{
    private final MoneyAccountRepository moneyAccountRepository;

    @Override
    @Transactional
    public MoneyAccount putMoney(Long id, BigDecimal money) {
        log.debug("Put money " + money + " to account with id = " + id);
        MoneyAccount moneyAccount = moneyAccountRepository.findById(id)
                .orElseThrow(() -> new AccountNotFoundException("Account with id = " + id + " not exist", id));
        moneyAccount.setMoney(moneyAccount.getMoney().add(money));
        return moneyAccountRepository.save(moneyAccount);
    }

    @Override
    @Transactional
    public MoneyAccount withdrawMoney(Long id, BigDecimal money) {
        log.debug("Withdraw money " + money + " from account with id = " + id);
        MoneyAccount moneyAccount = moneyAccountRepository.findById(id)
                .orElseThrow(() -> new AccountNotFoundException("Account with id = " + id + " not exist", id));
        if(moneyAccount.getMoney().compareTo(money) < 0) {
            throw new MoneyNotEnoughException("Not enough money in the account = " + id, id);
        }
        moneyAccount.setMoney(moneyAccount.getMoney().subtract(money));
        return moneyAccountRepository.save(moneyAccount);
    }

    @Override
    @Transactional
    public void transferringMoney(Long fromId, Long toId, BigDecimal money) {
        log.debug("Move money " + money + " from account with id = " + fromId + " to account with id = " + toId);

        Map<Long, MoneyAccount> accounts = moneyAccountRepository.findByIdIn(new ArrayList<Long>(Arrays.asList(fromId, toId)))
                .stream().collect(Collectors.toMap(MoneyAccount::getId, Function.identity()));

        MoneyAccount fromMoneyAccount = accounts.get(fromId);
        MoneyAccount toMoneyAccount = accounts.get(toId);

        if(fromMoneyAccount == null) {
            throw new AccountNotFoundException("Account with id = " + fromId + " not exist", fromId);
        }
        if(toMoneyAccount == null) {
            throw new AccountNotFoundException("Account with id = " + toId + " not exist", toId);
        }
        if(fromMoneyAccount.getMoney().compareTo(money) < 0) {
            throw new MoneyNotEnoughException("Not enough money in the account = " + fromId, fromId);
        }

        fromMoneyAccount.setMoney(fromMoneyAccount.getMoney().subtract(money));
        toMoneyAccount.setMoney(toMoneyAccount.getMoney().add(money));

        moneyAccountRepository.save(fromMoneyAccount);
        moneyAccountRepository.save(toMoneyAccount);
    }
}
